function [ ave ] = sampleMean( data, num )

n = size(data,1);
idx = randperm(n);
idx = idx(1:num);

D = data(idx,:);

sum = 0;

for i=1:num
    for j=1:num
        dif = D(i,:) - D(j,:);
        sum = sum + ( dif * dif');
    end
end

ave = sum / (num*num);

end

