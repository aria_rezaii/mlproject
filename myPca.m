function [ redD, score ] = myPca( D, threshold )
% redD = reduced data
% score = eigen values for selected principal components
% D = initial dataset (rows = data, columns = features)
% threshold = threshold for energy to choose

% Use SVD
[~,d] = size(D);
D = zscore(D);
sigma = 1/d * (D' * D);

% Allocate eigen values and eigen vectors
[eigVec,eigVal,~] = svd(sigma,0);
eigVal = diag(eigVal);

% Select top scoring eigenvectors
prop = zeros(1,d);
for i=1:d
    prop(i) = eigVal(i) / max(eigVal);
end
idx = find(prop > threshold);

% Transform data
score = prop(idx);
redD = (eigVec(:,idx)' * D')';
redD(redD < eps) = 0;
end

