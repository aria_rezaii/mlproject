thresholds = [ 0.5 0.1  ]



for i = 1:size(thresholds, 2)
    load(strcat( 'kmean_data/kmean_pca_threshold_', num2str(thresholds(i)), '.mat'));
    save(strcat( 'kmean_data/kmean_centroids_pca_threshold_', num2str(thresholds(i)), '.mat'), 'centroids','thresholds');
end
