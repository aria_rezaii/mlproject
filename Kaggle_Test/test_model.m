function [ predicted_label, IDs ] = test_model( model )
%TEST_MODEL Summary of this function goes here
%   Getting the trained model via svmtrain and test data, it will predict
%   labels of these data.
%   model: trained model via svmtrain
%   predicted_label: labels which had been predicted for trD based on model
    
    addpath('../libsvm-3.21/matlab/')
    addpath('../')
    which svmtrain
    
    trD = csvread('../data/test.csv',1);
    
    IDs = trD(:,1);
    trD = trD(:,2:end);
    m = size(trD,1);
    does_not_matter = zeros(m,1);
    
    model
    [predicted_label] = svmpredict(does_not_matter, trD, model );
    
end

