clear
thresholds = [ 0.5 0.1 0.01]

configure = [ [16.0000,37.7653];
              [2.0000,0.1175];
              [1024.0000,0.0640]
              ];

addpath('../');
addpath ../libsvm-3.21/matlab/
which svmtrain
for i = 1:size(thresholds, 2)
    % sample kmean_pca_threshold_0.01_wposXata.mat
    load(strcat( '../kmean_data/kmean_pca_threshold_', num2str(thresholds(i)), '_wposXata_no2', '.mat'));
    
%     resFile = fopen(strcat('svm_kmean_threshold_',num2str(thresholds(i)),'_res_no2.txt'),'w');
    sampleSize = 1000;
    k = 5;
    normalization = 1;
%     posX = posX_new;
    % Gathering data
    fprintf('Gathering data ...');
%     read;
    fprintf('[DONE]\n');
    
    data = [ posX_new; centroids ];
    labels = [ones(size(posX_new,1),1); zeros(size(centroids,1),1)];
    fprintf('[DONE]\n');
% 
    normalization = 1;
    if (normalization == 1)
        data = zscore(data);
    end

    % Initiating experiment
    fprintf('Initiating SVM grid search\n');
    
    C = configure(i,1);
    gamma = configure(i,2);
    options = sprintf('-t 2 -c %f -g %f', C, gamma);
    model = svmtrain(labels, data, options);
    fprintf('[DONE]\n');
     [ predicted_label, IDs ] = test_model( model);
    save(['kmean_temp_', thresholds(i)])
    save_labels(predicted_label, IDs, ['kmean_2_' ,num2str(thresholds(i))]);
    save(['kmean_temp_model_', thresholds(i)])

    fprintf('Experiment ended.\n');

end
