clear
addpath('../libsvm-3.21/matlab/')
addpath('../Hard Mining/')
which svmtrain

thresholds = [ 0.5; 0.1; 0.01];
configure = [ [16 , 547900009151855168.00];
              [1024.00, 4383200073214841344.00]
              [16.00, 68487501143981896.00]
              ];

my_read
trD_original = trD;

random_size = 3000;

filename = 'HardMining_results_ans_PCA_';


acc = [];
for j = 1:size(thresholds,1)
    trD = myPca(trD_original, thresholds(j));
    C     = configure(j,1);
    gamma = configure(j,2);
  
    [posD, pos_size, negD, neg_size] = findPosData(trD, trLbl);
    perm = randperm(neg_size)';

    non_SVs = [];
    SVs = [];
    acc = [];
    for i = 1:5
        tic
        
        negD_indexes = perm(1:random_size, :);
        perm = perm(random_size+1:end ,:);

        negD_step = [trD(negD_indexes,:) ; trD(SVs,:)];
        data = [posD ; negD_step];
        labels = [ones(pos_size,1) ; zeros(size(negD_step,1),1)];


        options = sprintf('-t 2 -c %f -g %f -q', C, gamma);
        model = svmtrain(labels, data, options);

        model_SVs = model.sv_indices;
        temp = transpose(1:size(data,1));
        [non_SVs_this_step, SVs_this_step] = find_nonSVs(model_SVs, temp, pos_size, [negD_indexes;SVs]);

        SVs = [SVs_this_step];
        non_SVs = [non_SVs;non_SVs_this_step];


        negD_step = trD(SVs,:);
        data = [posD ; negD_step];
        labels = [ones(pos_size,1) ; zeros(size(negD_step,1),1)];

        C, gamma, i

        toc

    end
    
    
    
%     save('temp')
%     load('temp')
    options = sprintf('-t 2 -c %f -g %f -q', C, gamma);
    model = svmtrain(labels, data, options);
    model

    [ predicted_label, IDs ] = test_model( model);
    
%     save('predicts_ready')
    save_labels(predicted_label, IDs, ['HardMining' ,num2str(thresholds(j))]);
    

end