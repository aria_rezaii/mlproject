trD = csvread('data/trD.csv');
featureNum = size(trD,2);
trIds = trD(:,1);
trLbl = trD(:,featureNum);
trD = trD(:,2:featureNum-1);

addpath libsvm-3.21/matlab/