function [ normD ] = dnorm( D )
% Normalizes the data, 
% Columns are features
% Rows are observations

n = size(D,1);
minD = repmat(min(D),n,1);
maxD = repmat(max(D),n,1);

normD = ( D - minD ) ./ ( maxD - minD );

end

