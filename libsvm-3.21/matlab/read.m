trD = csvread('../../data/trD.csv');
featureNum = size(trD,2);
trIds = trD(:,1);
trLbl = trD(:,featureNum);
trD = trD(:,2:featureNum-1);

% addpath libsvm-3.21/matlab/

thresholds = [ 0.5 0.1 0.01];
i = 1;

addpath('../');
load(strcat( '../../kmean_data/kmean_pca_threshold_', num2str(thresholds(i)), '_wposXata_no2', '.mat'));
resFile = fopen(strcat('hardmining_kmeanData_threshold_',num2str(thresholds(i)),'_res_no1.txt'),'w');
posX = posX_new;

fprintf('Gathering data ...');
% read;
fprintf('[DONE]\n');

data = [ posX; centroids ];
labels = [ones(size(posX,1),1); zeros(size(centroids,1),1)];
fprintf('[DONE]\n');

% if (normalization == 1)
%     data = zscore(data);
% end