function [out, in] = find_nonSVs( SVs, original, pos_size, negD_indexes )
%FIND_NONSVS Summary of this function goes here
%   Detailed explanation goes here
%     original = sort(original);
%     SVs = sort(SVs);
%     n = size(SVs,1);
    
    
   indexes = find(~ismember(original,SVs));
   indexes = indexes(indexes > pos_size, :);
   out = negD_indexes(indexes - pos_size);
%    out = find(ismember(negD_indexes,indexes))

   indexes_2 = find(ismember(original,SVs));
   indexes_2 = indexes_2(indexes_2> pos_size, :);
   in = negD_indexes(indexes_2 - pos_size);
end

