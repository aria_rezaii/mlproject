clear
addpath('../libsvm-3.21/matlab/')
addpath('../')
which svmtrain

thresholds = [ 0.01]
my_read
trD_original = trD;

non_SVs = [];
SVs = [];
random_size = 3000;
% C = 128;
% gamma = 0.2894;

filename = 'HardMining_results_ans_PCA_0.01';
initG = sampleMean(trD, 2000);
acc = [];
for j = 1:size(thresholds,1)
    trD = myPca(trD_original, thresholds(j));
    generate_C_gamma;
    


    for cIdx=1:size(potC,2)
        for gIdx=1:size(potG,2)
            C = potC(cIdx);  % Current C
            gamma = potG(gIdx);  % Current gamma

            [posD, pos_size, negD, neg_size] = findPosData(trD, trLbl);
            perm = randperm(neg_size)';
            
            non_SVs = [];
            SVs = [];
            for i = 1:5
                tic

            %     [bestC, bestG, bestAcc] = svmGrid(data, labels, k, sampleSize, resFile);
                negD_indexes = perm(1:random_size, :);
                perm = perm(random_size+1:end ,:);

                negD_step = [trD(negD_indexes,:) ; trD(SVs,:)];
                data = [posD ; negD_step];
                labels = [ones(pos_size,1) ; zeros(size(negD_step,1),1)];


                options = sprintf('-t 2 -c %f -g %f -q', C, gamma);
                model = svmtrain(labels, data, options);

                model_SVs = model.sv_indices;
                temp = transpose(1:size(data,1));
                [non_SVs_this_step, SVs_this_step] = find_nonSVs(model_SVs, temp, pos_size, [negD_indexes;SVs]);

                SVs = [SVs_this_step];
                non_SVs = [non_SVs;non_SVs_this_step];



                negD_step = trD(SVs,:);
                data = [posD ; negD_step];
                labels = [ones(pos_size,1) ; zeros(size(negD_step,1),1)];

                options = sprintf('-t 2 -v 5 -c %f -g %f -q', C, gamma);
                model = svmtrain(labels, data, options);
                acc =  [  acc ; [thresholds(j) i size( SVs,1) model ]]

                fprintf(filename, '(%d, %.2f, %.2f, %d) -> %.2f\n', thresholds(j), C, gamma, size( SVs,1), model);
                C, gamma, i
            %     size( SVs,1)
                toc
                csvwrite('resutls_PCA_0.01', acc);
            end
        end
    end
end