
% addpath('../');
load(strcat( '../kmean_data/kmean_pca_threshold_', num2str(thresholds(i)), '_wposXata_no2', '.mat'));
resFile = fopen(strcat('hardmining_kmeanData_threshold_',num2str(thresholds(i)),'_res_no1.txt'),'w');
posX = posX_new;

fprintf('Gathering data ...');
% my_read;
fprintf('[DONE]\n');

data = [ posX; centroids ];
labels = [ones(size(posX,1),1); zeros(size(centroids,1),1)];
fprintf('[DONE]\n');

% if (normalization == 1)
%     data = zscore(data);
% end