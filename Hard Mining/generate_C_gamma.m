maxC = 10;
minC = 4;
maxG = 3;
minG = -6;
w = 3;
potC = (2 * ones(1,(maxC-minC)/w+1)) .^ (minC:w:maxC);
potG = (2 * ones(1,(maxG-minG)/w+1)) .^ (minG:w:maxG) * initG;