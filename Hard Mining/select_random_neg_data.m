function indexes = select_random_neg_data( random_size, non_SVs, negD_indexes_all )
%SELECT_RANDOM_NEG_DATA Summary of this function goes here
%   Detailed explanation goes here
    negD_indexes_all(non_SVs,:) = [];
    indexes = randsample( size(negD_indexes_all,1), random_size);
    
end

