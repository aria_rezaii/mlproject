clear
addpath('../libsvm-3.21/matlab/')
addpath('../')
which svmtrain

thresholds = [ 0.5]
my_read
trD_original = trD;

non_SVs = [];
SVs = [];
random_size = 3000;
% C = 128;
% gamma = 0.2894;

filename = 'HardMining_results_ans_PCA_0.5';
initG = sampleMean(trD, 2000);
acc = [];
for j = 1:size(thresholds,1)
    trD = myPca(trD_original, thresholds(j));
    generate_C_gamma;
    [posD, pos_size, negD, neg_size] = findPosData(trD, trLbl);
    perm = randperm(neg_size)';


    for cIdx=1:size(potC,2)
        for gIdx=1:size(potG,2)
            C = potC(cIdx);  % Current C
            gamma = potG(gIdx);  % Current gamma


            for i = 1:5
                tic

            %     [bestC, bestG, bestAcc] = svmGrid(data, labels, k, sampleSize, resFile);
               
                
                if i == 1
                    negD_indexes = perm(1:3006, :);
                    perm = perm(3006+1:end ,:);
                    
                    negD_step = [trD(negD_indexes,:) ];
                else
                    negD_indexes = perm(1:500, :);
                    perm = perm(500+1:end ,:);
                    
                    perm_SV = randperm(size(SVs,1))';
                    SV_indexes = perm_SV(1:2500, :);
                    perm_SV = perm_SV(500+1:end ,:);
                    negD_step = [trD(negD_indexes,:) ; trD(SVs(perm_SV,1),:)];
                end
                
                data = [posD ; negD_step];
                labels = [ones(pos_size,1) ; zeros(size(negD_step,1),1)];


                options = sprintf('-t 2 -c %f -g %f -q', C, gamma);
                model = svmtrain(labels, data, options);

                model_SVs = model.sv_indices;
                temp = transpose(1:size(data,1));
                [non_SVs_this_step, SVs_this_step] = find_nonSVs(model_SVs, temp, pos_size, [negD_indexes;SVs]);

                SVs = [SVs_this_step];
                non_SVs = [non_SVs;non_SVs_this_step];



                negD_step = trD(SVs,:);
                data = [posD ; negD_step];
                labels = [ones(pos_size,1) ; zeros(size(negD_step,1),1)];

                options = sprintf('-t 2 -v 5 -c %f -g %f -q', C, gamma);
                model = svmtrain(labels, data, options);
                acc =  [  acc ; [thresholds(j) i size( SVs,1) model ]]

                fprintf(filename, '(%d, %.2f, %.2f, %d) -> %.2f\n', thresholds(j), C, gamma, size( SVs,1), model);
                C, gamma, i
            %     size( SVs,1)
                toc
                csvwrite('resutls_PCA_0.5', acc);
            end
        end
    end
end