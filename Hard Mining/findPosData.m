function [posD, pos_size, negD, neg_size] = findPosData( trD, trLb )
%FINDPOSDATA Summary of this function goes here
%   Detailed explanation goes here
    
    posD = trD( trLb == 1, : );
    negD = trD( trLb ~= 1, :);
    pos_size = size(posD,1);
    neg_size = size(negD,1);
    
end

