function my_ans = removePositivesAndOverlaps( rects, ubAnno )
%REMOVEPOSITIVESANDOVERLAPS Summary of this function goes here
%   Detailed explanation goes here

    my_ans = [];
    n = size( rects, 2);
    m = size( ubAnno, 2);
    
    
    for i = 1:n
        flag = 0;
        for j = 1:m
            overlap = HW2_Utils.rectOverlap(rects(1:4,i), ubAnno(:,j));
            if overlap > 0.3
               flag = 1;
               break;
            end
        end
        if flag == 0
           my_ans(:,size(my_ans, 2) + 1 ) = rects(:,i);
        end
    end
end

