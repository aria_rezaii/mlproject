% Randomly chooses a number of negative points
% Executes grid search to find the best performing SVM accuracy
% Writes accuracy on a file

% Allocating result file
resFile = fopen('svm_rnd_res.txt','w');
sampleSize = 1000;
k = 5;
normalization = 1;

% Gathering data
fprintf('Gathering data ...');
read;
fprintf('[DONE]\n');
fprintf('Getting positive and random negative observations ...');
[D, Lbl] = getPosRandomNeg( trD, trLbl );
save('randD.mat', 'D');
fprintf('[DONE]\n');

if (normalization == 1)
    D = zscore(D);
end

% Initiating experiment
fprintf('Initiating SVM grid search\n');
[bestC, bestG, bestAcc] = svmGrid(D, Lbl, k, sampleSize, resFile);
fprintf('[DONE]\n');

% Finishing everything
fclose(resFile);
fprintf('Experiment ended.\n');