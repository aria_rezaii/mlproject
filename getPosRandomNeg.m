function [ D, Lbl ] = getPosRandomNeg( rawD, rawLbl )

[posD, negD] = sepPosNeg(rawD, rawLbl);
posSize = size(posD,1);
idx = randperm(size(negD,1));
idx = idx(1:posSize);

D = [posD; negD(idx,:)];
Lbl = [ones(posSize,1); zeros(posSize,1)];


end

