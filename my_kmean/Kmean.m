clc

% X = load('../../hw3data/digits/digit.txt');
% Y = load('../../hw3data/digits/labels.txt');
M = csvread('../data/train.csv',1 ,1);

[row_num, col_num ] = size(M);

Y = M(:, col_num);
X = M( Y(:,1) == 0 , 1:col_num-1);

results = [];

K = 3008;
sum = [ 0 0 0 0 ]; 
% for j = 1:10
tic
isRandom  = true
[centers, count, labels] = Kmean_func(X, isRandom, K);
% [labels, centroids] = kmeans( X, K);
ss_val = SS(X, centroids, labels)
[p1,p2,p3] = pair_counting_measure(X, labels, Y)
sum = sum + [ ss_val p1 p2 p3];
toc
% end
results = [results; sum]
save('average_results_project_k_3008_myKmean');

% A = [1 2 3; 1 0 1];


% results = results / 10;
% save('average_results_project_k_3000');
% load('average_results_project2');
% plot( 1:10 , results(:,1));
% plot( 1:10 , results(:,2) , 'g', 1:10 , results(:,3) , 'r', 1:10 , results(:,4) , 'b' );


