function centers = centers_update( X, labels, K_num )
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    centers = zeros(K_num,size(X,2));
    n = size(X,1);
    for i = 1:K_num
        sum = zeros(1,size(X,2));
        count = 0;
        for j = 1:n
            if labels(j,1) == i
               count = count + 1;
               sum = sum + X(j,:); 
            end
        end
        centers(i,:) = sum / count;
    end

end

