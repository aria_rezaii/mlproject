function [ centers, counter, labelAssignment ] = Kmean_func( X, isRandom, K )
%KMEAN_FUNC Summary of this function goes here
%   Detailed explanation goes here


    % Step 1
    centers = centers_init( X, isRandom, K);

    counter = 1;
    isConverge = false;
%     centers
%     counter
    while counter < 21 && isConverge == false
        tic
        % Step 2
        labelAssignment = findNearestCenter( X, centers);

        % Step 3
        old_centers = centers;
        centers = centers_update(X, labelAssignment, K);
        
        counter
        counter = counter + 1;
        if old_centers == centers
            isConverge = true;
            counter = counter - 1;
        end
        toc
%         centers
        
    end
    counter = counter - 1;
%     centers
    counter

end

