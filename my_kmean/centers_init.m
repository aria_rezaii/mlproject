function centers = centers_init( X, isRandom, K )
%CENTERSINIT Summary of this function goes here
%   Detailed explanation goes here
%     centers = zeros(K,size(X,2));
    if isRandom == false
        centers = X(1:K,:);
    else
        random_rows = randperm(size(X,1))';
        centers = X( random_rows(1:K,:), :);
    end
    
    
end

