function labels = findNearestCenter( X, centers )
%FINDNEARESTCENTER Summary of this function goes here
%   Detailed explanation goes here
    labels = zeros( size(X,1) , 1);

    centers_num = size(centers,1);
    n = size(X,1);

    for i = 1:n
        temp_label = 0;
        min_distance = inf;
        for j = 1:centers_num
            temp_distance = distance( X(i,:), centers(j,:));
            if min_distance > temp_distance
                min_distance = temp_distance;
                temp_label = j;
            end
        end
        labels(i,1) = temp_label;
    end
end

