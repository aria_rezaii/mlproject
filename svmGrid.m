function [ bestC, bestG, bestAcc ] = svmGrid( D, Lbl, k, sampleSize, fileName )
bestAcc = -1;
bestG = 0;
bestC = 0;

maxC = 10;
minC = -2;
w = 3; % The size of intervals
maxG = 3;
minG = -12;

fprintf('\tFinding mean euclidean distance...');
initG = sampleMean(D, sampleSize);
fprintf('[DONE]\n');

potC = (2 * ones(1,(maxC-minC)/w+1)) .^ (minC:w:maxC);
potG = (2 * ones(1,(maxG-minG)/w+1)) .^ (minG:w:maxG) * initG;

for cIdx=1:size(potC,2)
    for gIdx=1:size(potG,2)
        curC = potC(cIdx);  % Current C
        curG = potG(gIdx);  % Current gamma
        options = sprintf('-t 2 -v %f -c %f -g %f -q',k,curC, curG);
        
        fprintf('\tTesting with C = %.2f and gamma = %.2f\n',curC, curG);
        curAcc = svmtrain(Lbl, D, options);
        fprintf(fileName, '(%.2f, %.2f) -> %.2f\n', curC, curG, curAcc);
        if (curAcc > bestAcc)
            bestAcc = curAcc;
            bestG = curG;
            bestC = curC;
        end
    end
end
fprintf(fileName,'Best pair = (%.4f,%.4f)\n',bestC,bestG);
end