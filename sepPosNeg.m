function [ posD, negD ] = sepPosNeg( D, Lbl )
% This function separates data based on binary labels 0 and 1.
% 0 is negative
% 1 is positive

posD = D(Lbl == 1,:);
negD = D(Lbl == 0,:);

end

