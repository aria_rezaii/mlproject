% Randomly chooses a number of negative points
% Executes grid search to find the best performing SVM accuracy
% Writes accuracy on a file

% Allocating result file
totResFile = fopen('pca_res.txt','w');
fprintf(totResFile, 'Threshold\tC\tGamma\tAccuracy\n');
sampleSize = 1000;
k = 5;

% Gathering data
fprintf('Gathering data ...');
D = ml_load('randD.mat', 'D');
posN = size(D,1) / 2;
Lbl = [ones(posN,1); zeros(posN,1)];
fprintf('[DONE]\n');

% Checking PCA performance
potThrs = [0.5, 0.1, 0.01];
for i=1:3
    fprintf('Experimenting with threshold = %.2f\n',potThrs(i));
    resFile = fopen(sprintf('svm_pca_%03d.txt',potThrs(i) * 100), 'w');
    rdcD = myPca(D, potThrs(i));
    [bestC, bestG, bestAcc] = svmGrid(rdcD, Lbl, k, sampleSize, resFile);
    fprintf(totResFile,'%.2f\t%.4f\t%.4f\t%.2f\n',potThrs(i), bestC, bestG, bestAcc);
    fclose(resFile);
end