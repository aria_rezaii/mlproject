clear
thresholds = [ 0.5 0.1 0.01]

for i = 1:size(thresholds, 2)
    % sample kmean_pca_threshold_0.01_wposXata.mat
    load(strcat( 'kmean_data/kmean_pca_threshold_', num2str(thresholds(i)), '_wposXata_no2', '.mat'));
    
    resFile = fopen(strcat('svm_kmean_threshold_',num2str(thresholds(i)),'_res_no2.txt'),'w');
    sampleSize = 1000;
    k = 5;
    normalization = 1;
    posX = posX_new;
    % Gathering data
    fprintf('Gathering data ...');
    read;
    fprintf('[DONE]\n');
    
    data = [ posX; centroids ];
    labels = [ones(size(posX,1),1); zeros(size(centroids,1),1)];
    fprintf('[DONE]\n');

    if (normalization == 1)
        data = zscore(data);
    end

    % Initiating experiment
    fprintf('Initiating SVM grid search\n');
    [bestC, bestG, bestAcc] = svmGrid(data, labels, k, sampleSize, resFile);
    fprintf('[DONE]\n');

    % Finishing everything
    fclose(resFile);
    fprintf('Experiment ended.\n');

end
