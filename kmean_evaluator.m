% extracting top features with different threshold using PCA
% finding kmean ( k = 3008 ) through all of negative data
% experimenting using centroids as our negative data

clear
addpath('my_kmean/');
% thresholds = [ 0.5 0.1 0.01]


% thresholds = [ 0.5 0.1 0.01 ]


M = csvread('data/train.csv',1 ,1);
[row_num, col_num ] = size(M);

Y = M(:, col_num);
posX = M( Y(:,1) == 1 , 1:col_num-1);
negX = M( Y(:,1) == 0 , 1:col_num-1);
Lb1 = [ones(size(posX,1),1); zeros(size(negX,1),1)];


% for i = 1:size(thresholds, 2)
%     [ redD, score ] = myPca([posX; negX], thresholds(i));
    
%     posX_new = redD(1:size(posX,1),:);
%     negX_new = redD((size(posX,1)+1):size(redD,1),:);
%     'PCA_Finished'
%     size(redD)

%     K = size(posX_new, 1)
    K = size(posX, 1);
%     [idx, centroids] = kmeans(negX_new, K, 'EmptyAction', 'singleton');
isRandom = 'true'
K 
% negX
    [ centroids, counter, labelAssignment ] = Kmean_func( negX, isRandom, K )
%     [idx, centroids] = kmeans(negX, K, 'EmptyAction', 'singleton');

    'Kmeans_finished'
    save(strcat( 'kmean_data/kmean_NO_pca_wposXata_no2.mat'), 'centroids', 'posX_new');
% end
